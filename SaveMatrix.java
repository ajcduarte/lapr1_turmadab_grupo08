package LAPR1_Final;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;

/**
 *
 * @author Ninja
 */


public class SaveMatrix {
    public static boolean saveAs(String path, String description, int[][] matrix) throws FileNotFoundException{
        Formatter fOut;
        try{
            fOut = new Formatter(new File(path));
        }catch (FileNotFoundException FileCannotBeCreated) {
            return false;
        }
        int x, y;
        
        fOut.format("%s%s%d%n",description,"  ",matrix.length);
        for(x=0;x<matrix.length;x++){
            fOut.format("%n%d",matrix[x][0]);
            for(y=1;y<matrix.length;y++){
                fOut.format("%s%d",",",matrix[x][y]);
            }
        }
        fOut.close();
        return true;
    }
    
    public static void main(String[] args) throws FileNotFoundException {
        int[][] matrix = {{1, 2, 3, 4},{5, 6, 7, 8},{9, 10, 11, 12},{13, 14, 15, 16}};
        saveAs("C:\\Users\\jinno\\Desktop\\Teste_matriz.txt","Imagem Azul",matrix);
    }
}
