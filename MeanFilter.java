package LAPR1_Final;

/**
 *
 * @author Ninja
 */
public class MeanFilter {
    public static void meanFilter(int size, int[][] matrix){
        int x, y, inner_x, inner_y, count_x, count_y, soma;
        int[][] new_matrix = new int[size][size];
        
        for(x=0;x<size;x++){
            for(y=0;y<size;y++){
                soma = 0;
                count_x = 0;
                if(x>0){inner_x=x-1;}else{inner_x=0;count_x++;}
                do{
                    count_y = 0;
                    if(y>0){inner_y=y-1;}else{inner_y=0;count_y++;}
                    do{
                        if((x==0 || x==size-1) && (inner_x==0 || inner_x==size-1)){soma+=matrix[inner_x][inner_y];} //CASO DUPLIQUEM NUMEROS DAS BORDERS;
                        if((y==0 || y==size-1) && (inner_y==0 || inner_y==size-1)){soma+=matrix[inner_x][inner_y];} //CASO DUPLIQUEM NUMEROS DAS BORDERS;
                        soma+=matrix[inner_x][inner_y];
                        inner_y++;
                        count_y++;
                    }while(inner_y<size && count_y<3);
                    inner_x++;
                    count_x++;
                }while(inner_x<size && count_x<3);
                if(x==0){count_x--;}if(y==0){count_y--;}
                new_matrix[x][y]=soma/9; //CASO DUPLIQUEM NUMEROS DAS BORDERS; SENAO, USAR soma/(count_x * count_y);
            }
        }
        
        for(x=0;x<size;x++){
            for(y=0;y<size;y++){
                System.out.printf("%2d", new_matrix[x][y]);
            }
            System.out.println();
        }
    }
    
    public static void main(String[] args){
        int[][] matrix = {{24,65,2,8},{23,51,2,7},{10,77,2,37},{99,42,2,7}};
        meanFilter(matrix.length,matrix);
    }
}
